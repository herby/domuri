/*
	domuri
	Wrap document.createElement("a") DOM-based URI handling.
	https://github.com/herby/domuri
	Copyright (c) 2014 Herbert Vojčík
	MIT License
*/

(function () {
	'use strict';
	var domuri = function (uri) {
        var element = document.createElement("a");
        element.href = uri || "";
        return element;
	};

	if (typeof define === 'function' && define.amd) {
		define([], function () { return domuri; });
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = domuri;
	} else {
		window.domuri = domuri;
	}
})();
